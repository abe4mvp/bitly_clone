class AddIndexesToShortenedUrls < ActiveRecord::Migration
  def change
    remove_index :shortened_urls, [:short_url, :user_id]
    add_index :shortened_urls, :short_url
    add_index :shortened_urls, :user_id
  end
end
