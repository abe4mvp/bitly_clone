class AddIndexToVisits < ActiveRecord::Migration
  def change
    add_index :visits, [:user_id, :shortened_url_id]
  end
end
