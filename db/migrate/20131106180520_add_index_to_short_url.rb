class AddIndexToShortUrl < ActiveRecord::Migration
  def change
    add_index :shortened_urls, [:short_url, :user_id]
  end
end
