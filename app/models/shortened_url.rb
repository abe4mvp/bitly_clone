class ShortenedUrl < ActiveRecord::Base

  attr_accessible :long_url, :short_url, :user_id

  belongs_to(
    :user,
    :class_name => "User",
    :foreign_key => :user_id,
    :primary_key => :id
  )

  has_many(
    :visits,
    :class_name => "Visit",
    :foreign_key => :shortened_url_id,
    :primary_key => :id
  )

  has_many :visitors, through: :visits, source: :users, uniq: true

  validates :short_url, :long_url, :uniqueness => true, :presence => true

  def self.random_code
    while true
      code = SecureRandom::urlsafe_base64
      return code unless find_by_short_url(code)
    end
  end

  def self.create_for_user_and_long_url!(long_url, user_id)
    ShortenedUrl.create(long_url: long_url, user_id: user_id, short_url: random_code)
  end

  def submitter
    ShortenedUrl.find_by_user_id(user_id)
  end

  def clicks
    Visit.where(shortened_url_id: self.id)
  end

  def num_clicks
    clicks.count
  end

  def num_uniques
    clicks.count(:user_id, distinct: true)
  end

  def num_recent_unique
    clicks.where(["created_at > ?", 10.minutes.ago]).count(:user_id, distinct: true)
  end

end