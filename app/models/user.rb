class User < ActiveRecord::Base

  attr_accessible :email

  has_many(
    :urls,
    :class_name => "ShortenedUrl",
    :foreign_key => :user_id,
    :primary_key => :id
  )

  has_many(
    :visits,
    :class_name => "Visit",
    :foreign_key => :user_id,
    :primary_key => :id
  )

  has_many :visited_urls, through: :visits, source: :shortened_urls

  validates :email, :uniqueness => true, :presence => true

  def submitted_urls
    ShortenedUrl.where(user_id: self.id)
  end
end
